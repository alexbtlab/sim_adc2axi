vlib modelsim_lib/work
vlib modelsim_lib/msim

vlib modelsim_lib/msim/xil_defaultlib
vlib modelsim_lib/msim/xpm

vmap xil_defaultlib modelsim_lib/msim/xil_defaultlib
vmap xpm modelsim_lib/msim/xpm

vlog -work xil_defaultlib -64 -incr -sv "+incdir+../../../../sim_ADC2AXI.srcs/sources_1/bd/design_1/ipshared/c923" \
"C:/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \

vcom -work xpm -64 -93 \
"C:/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_VCOMP.vhd" \

vlog -work xil_defaultlib -64 -incr "+incdir+../../../../sim_ADC2AXI.srcs/sources_1/bd/design_1/ipshared/c923" \
"../../../bd/design_1/ipshared/fabb/src/ADC2AXI_v1_0_M00_AXIS.v" \
"../../../bd/design_1/ipshared/fabb/src/ram_4096_v1_0.v" \
"../../../bd/design_1/ipshared/fabb/src/ram_control_v1_0.v" \
"../../../bd/design_1/ipshared/fabb/src/ADC2AXI_v1_0.v" \
"../../../bd/design_1/ip/design_1_ADC2AXIS_0_0/sim/design_1_ADC2AXIS_0_0.v" \
"../../../bd/design_1/sim/design_1.v" \
"../../../bd/design_1/ipshared/de74/hdl/counter_v1_0.v" \
"../../../bd/design_1/ip/design_1_counter_0_0/sim/design_1_counter_0_0.v" \
"../../../bd/design_1/ip/design_1_clk_wiz_0_0/design_1_clk_wiz_0_0_clk_wiz.v" \
"../../../bd/design_1/ip/design_1_clk_wiz_0_0/design_1_clk_wiz_0_0.v" \
"../../../bd/design_1/ip/design_1_clk_wiz_1_0/design_1_clk_wiz_1_0_clk_wiz.v" \
"../../../bd/design_1/ip/design_1_clk_wiz_1_0/design_1_clk_wiz_1_0.v" \

vlog -work xil_defaultlib \
"glbl.v"

