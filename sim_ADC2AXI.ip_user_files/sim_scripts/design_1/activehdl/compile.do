vlib work
vlib activehdl

vlib activehdl/xil_defaultlib
vlib activehdl/xpm

vmap xil_defaultlib activehdl/xil_defaultlib
vmap xpm activehdl/xpm

vlog -work xil_defaultlib  -sv2k12 "+incdir+../../../../sim_ADC2AXI.srcs/sources_1/bd/design_1/ipshared/c923" \
"C:/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \

vcom -work xpm -93 \
"C:/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_VCOMP.vhd" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../sim_ADC2AXI.srcs/sources_1/bd/design_1/ipshared/c923" \
"../../../bd/design_1/ipshared/fabb/src/ADC2AXI_v1_0_M00_AXIS.v" \
"../../../bd/design_1/ipshared/fabb/src/ram_4096_v1_0.v" \
"../../../bd/design_1/ipshared/fabb/src/ram_control_v1_0.v" \
"../../../bd/design_1/ipshared/fabb/src/ADC2AXI_v1_0.v" \
"../../../bd/design_1/ip/design_1_ADC2AXIS_0_0/sim/design_1_ADC2AXIS_0_0.v" \
"../../../bd/design_1/sim/design_1.v" \
"../../../bd/design_1/ipshared/de74/hdl/counter_v1_0.v" \
"../../../bd/design_1/ip/design_1_counter_0_0/sim/design_1_counter_0_0.v" \
"../../../bd/design_1/ip/design_1_clk_wiz_0_0/design_1_clk_wiz_0_0_clk_wiz.v" \
"../../../bd/design_1/ip/design_1_clk_wiz_0_0/design_1_clk_wiz_0_0.v" \
"../../../bd/design_1/ip/design_1_clk_wiz_1_0/design_1_clk_wiz_1_0_clk_wiz.v" \
"../../../bd/design_1/ip/design_1_clk_wiz_1_0/design_1_clk_wiz_1_0.v" \

vlog -work xil_defaultlib \
"glbl.v"

