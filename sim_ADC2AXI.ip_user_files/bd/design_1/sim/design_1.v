//Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
//Date        : Sun Jun 14 15:07:18 2020
//Host        : DESKTOPAEV67KM running 64-bit major release  (build 9200)
//Command     : generate_target design_1.bd
//Design      : design_1
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CORE_GENERATION_INFO = "design_1,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=design_1,x_ipVersion=1.00.a,x_ipLanguage=VERILOG,numBlks=4,numReposBlks=4,numNonXlnxBlks=1,numHierBlks=0,maxHierDepth=0,numSysgenBlks=0,numHlsBlks=0,numHdlrefBlks=0,numPkgbdBlks=0,bdsource=USER,synth_mode=OOC_per_IP}" *) (* HW_HANDOFF = "design_1.hwdef" *) 
module design_1
   (clk_in1_0,
    frameSize_0,
    reset_0,
    reset_ram_0);
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 CLK.CLK_IN1_0 CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME CLK.CLK_IN1_0, ASSOCIATED_RESET reset_ram_0, CLK_DOMAIN design_1_clk_in1_0, FREQ_HZ 100000000, INSERT_VIP 0, PHASE 0.000" *) input clk_in1_0;
  input [15:0]frameSize_0;
  input reset_0;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 RST.RESET_RAM_0 RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME RST.RESET_RAM_0, INSERT_VIP 0, POLARITY ACTIVE_LOW" *) input reset_ram_0;

  wire clk_in1_0_1;
  wire clk_wiz_0_clk_out1;
  wire clk_wiz_1_clk_out1;
  wire [31:0]counter_0_count;
  wire counter_0_ready;
  wire [15:0]frameSize_0_1;
  wire reset_0_1;
  wire reset_ram_0_1;

  assign clk_in1_0_1 = clk_in1_0;
  assign frameSize_0_1 = frameSize_0[15:0];
  assign reset_0_1 = reset_0;
  assign reset_ram_0_1 = reset_ram_0;
  design_1_ADC2AXIS_0_0 ADC2AXIS_0
       (.data_ready(counter_0_ready),
        .frameSize(frameSize_0_1),
        .i_data(counter_0_count),
        .m00_axis_aclk(clk_in1_0_1),
        .m00_axis_aresetn(reset_ram_0_1),
        .reset_ram(reset_ram_0_1));
  design_1_clk_wiz_0_0 clk_wiz_0
       (.clk_in1(clk_in1_0_1),
        .clk_out1(clk_wiz_0_clk_out1),
        .reset(1'b0));
  design_1_clk_wiz_1_0 clk_wiz_1
       (.clk_in1(clk_wiz_0_clk_out1),
        .clk_out1(clk_wiz_1_clk_out1),
        .reset(1'b0));
  design_1_counter_0_0 counter_0
       (.FrameSize(frameSize_0_1),
        .clk(clk_wiz_1_clk_out1),
        .count(counter_0_count),
        .ready(counter_0_ready),
        .reset(reset_0_1));
endmodule
