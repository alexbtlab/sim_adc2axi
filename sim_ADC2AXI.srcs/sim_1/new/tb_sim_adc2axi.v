`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 14.06.2020 14:39:09
// Design Name: 
// Module Name: tb_sim_adc2axi
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_sim_adc2axi();
   
    reg [15:0]frameSize_0;
 reg reset_0;
    reg clk_in1_0;
    reg m00_axis_aresetn_0;
    reg reset_ram_0;

initial begin
    frameSize_0 = 16;
end  

  
initial begin
    reset_ram_0 = 0;
    #1000 reset_ram_0 = 1;
end  
initial begin
    clk_in1_0 = 0;
    forever #5 clk_in1_0 = ~clk_in1_0;
end

  
    design_1_wrapper DUT
   (
    .clk_in1_0(clk_in1_0),
    .frameSize_0(frameSize_0),
    .reset_0(reset_0),
    .reset_ram_0(reset_ram_0)
    );
 

endmodule
