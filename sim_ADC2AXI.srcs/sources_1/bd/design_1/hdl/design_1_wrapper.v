//Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
//Date        : Sun Jun 14 15:07:18 2020
//Host        : DESKTOPAEV67KM running 64-bit major release  (build 9200)
//Command     : generate_target design_1_wrapper.bd
//Design      : design_1_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module design_1_wrapper
   (clk_in1_0,
    frameSize_0,
    reset_0,
    reset_ram_0);
  input clk_in1_0;
  input [15:0]frameSize_0;
  input reset_0;
  input reset_ram_0;

  wire clk_in1_0;
  wire [15:0]frameSize_0;
  wire reset_0;
  wire reset_ram_0;

  design_1 design_1_i
       (.clk_in1_0(clk_in1_0),
        .frameSize_0(frameSize_0),
        .reset_0(reset_0),
        .reset_ram_0(reset_ram_0));
endmodule
