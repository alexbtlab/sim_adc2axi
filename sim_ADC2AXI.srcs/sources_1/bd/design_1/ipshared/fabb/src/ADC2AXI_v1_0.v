
`timescale 1 ns / 1 ps

	module ADC2AXI_v1_0 #
	(
		// Users to add parameters here

		// User parameters ends
		// Do not modify the parameters beyond this line

        parameter ADDR_WIDTH = 16, DATA_WIDTH = 32, DEPTH = 8192,
		// Parameters of Axi Master Bus Interface M00_AXIS
		parameter integer C_M00_AXIS_TDATA_WIDTH	= 32,
		parameter integer C_M00_AXIS_START_COUNT	= 32
	)
	(
		// Users to add ports here
        input wire [15:0] frameSize,
		// User ports ends
		// Do not modify the ports beyond this line
        input wire [31:0] i_data,
        input wire reset_ram,
        input wire data_ready,
		// Ports of Axi Master Bus Interface M00_AXIS
		input wire  m00_axis_aclk,
		input wire  m00_axis_aresetn,
		output wire  m00_axis_tvalid,
		output wire [C_M00_AXIS_TDATA_WIDTH-1 : 0] m00_axis_tdata,
		output wire [(C_M00_AXIS_TDATA_WIDTH/8)-1 : 0] m00_axis_tstrb,
		output wire  m00_axis_tlast
		//input wire  m00_axis_tready
	); 
	
	wire [15:0] adr_ram;
	wire WR;
	wire clk_write_read;
	wire clk_100MHz_AXI;
	
	ram_control ram_control_inst(
		.main_clk(m00_axis_aclk),
		.reset(reset_ram),
		.data_ready(data_ready),
		.FrameSize(frameSize),
		.clk_write_read(clk_write_read),
		.adr_ram(adr_ram),
		.WR(WR),
		.clk_100MHz_AXI(clk_100MHz_AXI)
	);
	
	
	wire [31:0] o_data;
	
	ram #(.ADDR_WIDTH(ADDR_WIDTH), 
	      .DATA_WIDTH(DATA_WIDTH), 
	      .DEPTH(DEPTH)) 
	ram_inst(
        .i_clk(clk_write_read),
        .i_addr(adr_ram), 
        .i_write(WR),
        .i_data(i_data),
        .o_data(o_data) 
    );
 
// Instantiation of Axi Bus Interface M00_AXIS
	ADC2AXI_v1_0_M00_AXIS # ( 
		.C_M_AXIS_TDATA_WIDTH(C_M00_AXIS_TDATA_WIDTH),
		.C_M_START_COUNT(C_M00_AXIS_START_COUNT)
	) ADC2AXI_v1_0_M00_AXIS_inst (
		.FrameSize(frameSize),
		.En(1'b1),
		.en_AXIclk(WR),
        .M_AXIS_ACLK(clk_100MHz_AXI),
		.M_AXIS_ARESETN(m00_axis_aresetn),
		.M_AXIS_TVALID(m00_axis_tvalid),
		.data_to_transmit(o_data),
		.M_AXIS_TDATA(m00_axis_tdata),
		.M_AXIS_TSTRB(m00_axis_tstrb),
		.M_AXIS_TLAST(m00_axis_tlast),
		.M_AXIS_TREADY(!WR)
	);

	// Add user logic here

	// User logic ends

	endmodule
